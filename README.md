# GitLab for Laboratory (assignments)

This project aims at simplifying the management of GitLab in the context of
classes. That is, creating groups for assignments, returning evaluations as
issues and so forth.

*Important notes*
-----------------

1. One needs to specify the `url` of the GitLab instance as well as the related
   `config_id` or `token`. The project recommends one of the following methods:

   1. `CFG` file

      If one creates a file in CFG format, this can be passed to any `gl-lab`
      command interacting with GitLab (with the `--config-file` option). The
      `CFG` file shall have the following content (please adapt where needed):
      ```bash
      [global]
      default = somewhere
      ssl_verify = true
      timeout = 5

      [somewhere]
      url = https://gitlab.somewhere.world
      private_token = 123456789ABCDEFGHIJKLMNOPR
      api_version = 4
      ```
      More information can be found under
      https://python-gitlab.readthedocs.io/en/stable/cli-usage.html.

   2. An `.env` file

      If one creates a `.env` file and summons `gl-lab` from the location of the
      file, the relevant information is loaded automatically. The `.env` file
      shall contain:
      ```bash
      GITLAB_URL="https://gitlab.somewhere.world/"
      GITLAB_TOKEN=123456789ABCDEFGHIJKLMNOPR
      ```
2. The project is actively developed yet not at version `1.0`. As a result, be
   aware that Major version zero (0.y.z) is for initial development. Anything
   MAY change at any time. The public API SHOULD NOT (yet) be considered stable.

## Usage example

Shuffle students from `studentlist.txt` in groups and save the result in a json file:

```bash
gl-lab shuffle studentlist.txt > tp01.json
```

Show a json group file as markdown (useful to produce document):

```bash
gl-lab to-md --group ado/test/sup/tp01 ./tp01.json
```
*Note* : it is necessary to specify the group to whom the students for this
assignment belong to. The above example makes use of the group created in *Create an assignment* below

Creates subgroup for a new year:

```bash
gl-lab mkgroup ado/2022-2023
```

Create a classroom with a teacher:

```bash
gl-lab mkgroup --name "Classe Jacques Supcik" ado/2022-2023/sup
```

Create an assignment:

```bash
gl-lab mkgroup ado/2022-2023/sup/tp01
```

Create all projects in a group from a json file. The name of the
projects will be the argument given with `--assignment` followed
by a dash `-` and a letter starting from `a`. So in the example
below, the projects will be named `tp01-a`, `tp01-b`, `tp01-c`...

```bash
gl-lab create-projects --group ado/test/sup/tp01 --assignment tp01 tp01.json
```

If the `--assignment` is not given, it is taken from the last element of the group
name. The following command makes the same as the previous one:

```bash
gl-lab create-projects --group ado/test/sup/tp01 tp01.json

```

## Clone a group

The following command clones all *GitLab* projects present in a subgroup -
typically used for retrieving assignment repositories :
```
gl-lab clone-group ado/2022-2023/sup/tp05
```

## Get all reports

Assuming you define a project structure in that all reports are to be stored
within a `docs` directory and the report document be called `report.pdf`, one
can thus retrieve all those documents and copy them by using one of the two
methods below :

```
for i in tp05/*/docs/report.pdf; do
    cp $i report-$(basename $(dirname $(dirname $i))).pdf
done
```

or

```
for i in tp04/*/docs/report04.pdf; do
    cp $i report-$(basename $(dirname $(dirname $i))).pdf
done
```

## Append evaluation

In case you have a dedicated pdf containing your report evaluation, you then
want to add it to the report. You can easily do so using the `gl-lab pdf-attach`
command to merge the files.

```
for i in report-*.pdf; do
    gl-lab pdf-attach $i ./INC/evaluation.pdf
done
```

## Return evaluation

In case the workflow uses issues for returning feedbacks to students, one can
easily accomplish this with `gl-lab` by using the `return-as-issue` function
as in the following example :

```
for i in {a..g}; do
    gl-lab return-as-issue ado/2022-2023/sup/tp05/tp05-${i} report-tp05-${i}_*.pdf
done
```
