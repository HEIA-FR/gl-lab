################################################################################
# @brief       : gl-lab - PDF manipulation
# @author      : Jacques Supcik <jacques.supcik@hefr.ch>
# @date        : 11 October 2023
# ------------------------------------------------------------------------------
# @copyright   : Copyright (c) 2023 HEIA-FR / ISC
#                Haute école d'ingénierie et d'architecture de Fribourg
#                Informatique et Systèmes de Communication
# @attention   : SPDX-License-Identifier: MIT OR Apache-2.0
################################################################################

import tempfile

import click
from PyPDF2 import PdfMerger


@click.command("pdf-attach", short_help="Merges multiple PDF files into a single one")
@click.argument("pdf", type=click.Path(exists=True), nargs=1)
@click.argument("attachments", type=click.Path(exists=True), nargs=-1)
@click.pass_context
def pdf_attach(ctx, pdf, attachments):
    """This command merges multiple PDF files into a single file - useful for
    example for inserting an evaluation into a report containing comments
    written by an assessor.

    Note: multiple files can be concatenated, not just 2. The resulting,
    concatenated file is the first one passed as argument (e.g.
    "report-group1-commented.pdf" in the example below).

    \b
    Usage example:
      gl-lab pdf-attach report-group1-commented.pdf assessment-group1.pdf"""

    merger = PdfMerger()
    merger.append(pdf)
    for f in attachments:
        merger.append(f)

    with tempfile.TemporaryFile() as tmp:
        merger.write(tmp)
        tmp.seek(0)
        with open(pdf, "wb") as fp:
            fp.write(tmp.read())

    merger.close()
