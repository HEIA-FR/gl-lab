################################################################################
# @brief       : gl-lab - Assignment management
# @author      : Jacques Supcik <jacques.supcik@hefr.ch>
# @date        : 11 October 2023
# ------------------------------------------------------------------------------
# @copyright   : Copyright (c) 2023 HEIA-FR / ISC
#                Haute école d'ingénierie et d'architecture de Fribourg
#                Informatique et Systèmes de Communication
# @attention   : SPDX-License-Identifier: MIT OR Apache-2.0
################################################################################

import json
import math
import random
import sys
import time
from collections import defaultdict
from pathlib import Path
from urllib.parse import urlparse

import click

MAX_TIME = 10  # seconds


@click.command("shuffle", short_help="Shuffles students into groups")
@click.option("--team-size", type=int, default=2, show_default=True, help="Team size.")
@click.option(
    "--prefer-more",
    "preference",
    flag_value="more",
    default=True,
    show_default=True,
    help="Round team composition to more people.",
)
@click.option(
    "--prefer-less",
    "preference",
    flag_value="less",
    help="Round team composition to less people.",
)
@click.option(
    "--seed",
    type=int,
    default=random.randrange(sys.maxsize),
    help="Seed to initialize randomness.",
)
@click.option(
    "--exclude",
    multiple=True,
    default=[],
    help="Exclude groups from a list in JSON.",
)
@click.option("--out", type=click.File("w"), default="-", help="Output filename.")
@click.argument("student-list", type=click.File("rt"))
@click.pass_context
def shuffle(  # noqa: C901,PLR0913
    ctx, team_size, preference, seed, exclude, out, student_list
):
    """This command shuffles students from a file (e.g.`studentlist.txt`) in
    groups and save the result in a json file.

    There are multiple options (e.g. team size, set per default to 2), make sure to check the list below.

    \b
    Usage examples:
      gl-lab shuffle studentlist.txt > tp01.json
      gl-lab shuffle --exclude excluded.json --out tp01.json studentlist.txt \n
      where excluded.json content is {"groups": [["name1"], ["name2"]]}. That
      means that no group with "name1" and "name2" will be created."""

    exclusion = dict()
    for p in exclude:
        for f in Path(".").glob(p):
            with f.open() as fp:
                t = json.load(fp)
                for g in t["groups"]:
                    for s in g:
                        exclusion[s] = exclusion.get(s, set()) | (set(g) - {s})

    students = [
        i for i in [line.rstrip() for line in student_list.readlines()] if len(i) > 0
    ]
    r = random.Random(seed)

    def same(solution):
        res = 0
        for g in solution:
            for s in g:
                if s in exclusion:
                    res += len((set(g) - {s}) & exclusion[s])
        return res

    t0 = time.monotonic()
    best_count = 0
    best = None
    while True:
        r.shuffle(students)
        nstudents = len(students)
        nteams = max(1, nstudents / team_size)
        if preference == "more":
            nteams = math.floor(nteams)
        else:
            nteams = math.ceil(nteams)

        pots = defaultdict(list)
        for i, s in enumerate(students):
            pots[i % nteams].append(s)

        bc = same(pots.values())
        if best is None or bc < best_count:
            best = list(pots.values())
            best_count = bc

        if bc == 0 or time.monotonic() - t0 > MAX_TIME:
            break

    click.echo(
        json.dumps(
            {"groups": [sorted(i) for i in best], "seed": seed, "score": best_count}
        ),
        file=out,
    )


@click.command("to-md", short_help="Converts a JSON assignment into a markdown table")
@click.option(
    "--gitlab-url", envvar="GITLAB_URL", default="https://gitlab.forge.hefr.ch/"
)
@click.option("--group", required=True, help="Relative gitlab path to assignment.")
@click.option("--assignment")
@click.argument("json-file", type=click.File("rt"))
@click.pass_context
def to_md(ctx, gitlab_url, group, assignment, json_file):
    """This command converts a JSON_FILE file containing an assignment to
    students into a markdown table - useful for inserting such a table into an
    existing document.

    \b
    Usage example:
      gl-lab to-md --group ado/test/sup/tp01 ./tp01.json"""
    p = Path(group)
    data = json.load(json_file)

    u = urlparse(gitlab_url)
    u = u._replace(path=str(Path(u.path) / group))
    if assignment is None:
        assignment = p.name

    for i, g in enumerate(data["groups"]):
        pname = "{}-{}".format(assignment, chr(ord("a") + i))
        users = ", ".join(g)
        click.echo(
            f"- [Groupe {i + 1:02d} / {pname}]({u._replace(path=str(Path(u.path) / pname)).geturl()}) ({users})"
        )
