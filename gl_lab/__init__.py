################################################################################
# @brief       : gl-lab - empty package file
# @author      : Jacques Supcik <jacques.supcik@hefr.ch>
# @date        : 11 October 2023
# ------------------------------------------------------------------------------
# @copyright   : Copyright (c) 2023 HEIA-FR / ISC
#                Haute école d'ingénierie et d'architecture de Fribourg
#                Informatique et Systèmes de Communication
# @attention   : SPDX-License-Identifier: MIT OR Apache-2.0
################################################################################
