################################################################################
# @brief       : GitLab toolbox for labs (assignments)
# @author      : Jacques Supcik <jacques.supcik@hefr.ch>
# @date        : 11 October 2023
# ------------------------------------------------------------------------------
# @copyright   : Copyright (c) 2023 HEIA-FR / ISC
#                Haute école d'ingénierie et d'architecture de Fribourg
#                Informatique et Systèmes de Communication
# @attention   : SPDX-License-Identifier: MIT OR Apache-2.0
################################################################################

import importlib.metadata
import logging

import click
import click_log
from dotenv import find_dotenv, load_dotenv

from .assignment import shuffle, to_md
from .gl import clone_group, create_projects, mkgroup, return_as_issue
from .pdf import pdf_attach

__version__ = importlib.metadata.version("gl-lab")
logger = logging.getLogger(__name__)
load_dotenv(  # take environment variables from .env.
    dotenv_path=find_dotenv(usecwd=True)
)


@click.group(invoke_without_command=True)
@click.option("--debug/--no-debug")
@click.option("--quiet/--no-quiet")
@click.option("--version", is_flag=True, help="Print version and exit.")
@click.pass_context
def cli(ctx, debug, quiet, version):
    ctx.ensure_object(dict)
    ctx.obj["DEBUG"] = debug

    root_logger = logging.getLogger()
    click_log.basic_config(root_logger)

    if debug:
        root_logger.setLevel(logging.DEBUG)
    elif quiet:
        root_logger.setLevel(logging.WARNING)
    else:
        root_logger.setLevel(logging.INFO)

    if version:
        click.echo(f"gl-lab version : {__version__}")
        ctx.exit()
    if ctx.invoked_subcommand is None:
        click.echo(ctx.get_help())


cli.add_command(pdf_attach)
cli.add_command(shuffle)
cli.add_command(to_md)
cli.add_command(mkgroup)
cli.add_command(create_projects)
cli.add_command(clone_group)
cli.add_command(return_as_issue)

if __name__ == "__main__":
    cli()
