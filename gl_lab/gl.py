################################################################################
# @brief       : gl-lab - API calls
# @author      : Jacques Supcik <jacques.supcik@hefr.ch>
# @date        : 11 October 2023
# ------------------------------------------------------------------------------
# @copyright   : Copyright (c) 2023 HEIA-FR / ISC
#                Haute école d'ingénierie et d'architecture de Fribourg
#                Informatique et Systèmes de Communication
# @attention   : SPDX-License-Identifier: MIT OR Apache-2.0
################################################################################

import json
import logging
import subprocess
from pathlib import Path

import click
import gitlab
from furl import furl

logger = logging.getLogger(__name__)
# Common options for gitlab interactions
gl_url = click.option(
    "--gitlab-url", envvar="GITLAB_URL", required=False, help="gitlab url"
)

gl_token = click.option(
    "--token", envvar="GITLAB_TOKEN", required=False, help="gitlab token"
)

gl_config_id = click.option(
    "--config-id", required=False, help="gitlab configuration id"
)

gl_config_file = click.option(
    "--config-file", required=False, help="gitlab configuration file"
)


def this_gitlab(ctx, gitlab_url, token, config_id, config_file):
    gl: gitlab.Gitlab = None

    def checked(f):
        def wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                logger.fatal(f"Error connecting to gitlab : {e}")
                ctx.exit(1)

        return wrapper

    ctx.obj["TOKEN"] = token

    if gitlab_url is not None and token is not None:
        logger.debug("Connecting to gitlab using gitlab_url and token")
        gl = checked(gitlab.Gitlab)(url=gitlab_url, private_token=token)
    elif config_file is not None:
        logger.debug("Connecting to gitlab using config-file")
        gl = checked(gitlab.Gitlab.from_config)(config_id, [config_file])
    elif config_id is not None:
        logger.debug("Connecting to gitlab using config-id")
        gl = checked(gitlab.Gitlab.from_config)(config_id)

    else:
        logger.fatal(
            "Error: you must provide either --gitlab-url and --token "
            "or --config-id and --config-file or --config-id"
        )
        ctx.exit(1)

    try:
        gl.auth()
    except Exception as e:
        logger.fatal(f"Error: cannot authenticate to gitlab : {e}")
        ctx.exit(1)

    if gl is None:
        logger.fatal("Error: cannot connect to gitlab")
        ctx.exit(1)

    logger.debug(f"Connected to {gl.url}")
    logger.debug(f"Logged in as {gl.user.username}")

    return gl


@click.command(
    "mkgroup", short_help="Creates a subgroup, class or assignment in GitLab"
)
@gl_url
@gl_token
@gl_config_id
@gl_config_file
@click.option("--name", help="Human readable, awesome name.")
@click.argument("path")
@click.pass_context
def mkgroup(  # noqa: PLR0913
    ctx, gitlab_url, token, config_id, config_file, name, path
):
    """This command creates a GitLab subgroup, a class or an assignment depending what
    the intention of the user is. Probably worth checking the examples below for
    a better understanding.

    \b
    Usage examples:
    # Creation of a GL subgroup
      gl-lab mkgroup topic/2023-2024\n
    \b
    # Create a classroom with a teacher
      gl-lab mkgroup --name "Jacques Shorthand Class" topic/2022-2023/short\n
    \b
    # Create an assignment
      gl-lab mkgroup topic/2023-2024/short/lab01"""

    gl = this_gitlab(ctx, gitlab_url, token, config_id, config_file)
    p = Path(path)

    parent = "/".join(p.parent.parts)
    if parent == "":
        logger.error(
            "You can only create subgroups. Make the top group using the web interface"
        )
        return

    if name is None:
        name = p.name

    try:
        parent = gl.groups.get(parent)
    except gitlab.exceptions.GitlabError as e:
        logger.error(f"Error getting parent group : {parent}")
        logger.error(e.error_message)
        return

    try:
        gl.groups.create({"name": name, "path": p.name, "parent_id": parent.id})
    except gitlab.exceptions.GitlabError as e:
        logger.error(f"Error making subfroup group : {p.name}")
        logger.error(e.error_message)
        return


@click.command("create-projects", short_help="Creates one or multiple GitLab projects")
@gl_url
@gl_token
@gl_config_id
@gl_config_file
@click.option("--group", required=True, help="Relative gitlab path to assignment.")
@click.option("--assignment", help="Assignment name.")
@click.option("--template-url", help="URL of the template project to use")
@click.option(
    "--inject-auth/--no-inject-auth",
    default=None,
    help="Inject authentication into the URL of the template project",
)
@click.argument("json-file", type=click.File("rt"))
@click.pass_context
def create_projects(  # noqa: PLR0912, PLR0913, PLR0915, C901
    ctx,
    gitlab_url,
    token,
    config_id,
    config_file,
    group,
    assignment,
    template_url,
    inject_auth,
    json_file,
):
    """
    The projects can be created from a
    template project. The URL can be a full URL of the form
    "https://<user:token>@<hostname>/<path>.git", but it can also be simpler.
    If you juste give the "path", the URL will be constructed from the gitlab
    URL used for the connection and will use the token provided with "--token".
    """
    gl = this_gitlab(ctx, gitlab_url, token, config_id, config_file)
    p = Path(group)
    data = json.load(json_file)

    if assignment is None:
        assignment = p.name

    # Get group (parent)
    parent = "/".join(p.parts)
    try:
        parent = gl.groups.get(parent)
    except gitlab.exceptions.GitlabError as e:
        logger.error(f"Error getting parent group : {parent}")
        logger.error(e.error_message)
        return

    # Get all users
    all_users = {}
    for t in data["groups"]:
        for u in t:
            if u in all_users:
                continue
            try:
                gl_user = gl.users.list(username=u)[0]
                all_users[u] = gl_user
            except IndexError:
                logger.error(f"User {u} not found")
                return
            except gitlab.exceptions.GitlabError as e:
                logger.error(f"Error reading user : {u}")
                logger.error(e.error_message)
                return

    # Create projects and add users
    for i, t in enumerate(data["groups"]):
        pname = f"{assignment}-{chr(ord('a') + i)}"
        args = {
            "name": pname,
            "namespace_id": parent.id,
            "initialize_with_readme": False,
        }
        if template_url is not None:
            f = furl(template_url)
            # Fix scheme and host if not provided
            if f.scheme is None:
                f.scheme = furl(gl.url).scheme
                if inject_auth is None:
                    inject_auth = True
            if f.host is None:
                f.host = furl(gl.url).host
                if inject_auth is None:
                    inject_auth = True
            if not str(f.path).endswith(".git"):
                f.path = str(f.path) + ".git"

            if inject_auth:
                f.username = gl.user.username
                if ctx.obj["TOKEN"] is not None:
                    f.password = ctx.obj["TOKEN"]
                else:
                    logger.warning("No token provided. Unable to inject password")

            args["import_url"] = f.url

        try:
            p = gl.projects.create(args)
        except gitlab.exceptions.GitlabError as e:
            logger.error(f"Error creating project : {pname}")
            logger.error(e.error_message)
            logger.error("Continuing with next project")
            continue

        for u in t:
            try:
                p.members.create(
                    {
                        "user_id": all_users[u].id,
                        "access_level": gitlab.const.AccessLevel.MAINTAINER,
                    }
                )
            except gitlab.exceptions.GitlabError as e:
                logger.error(f"Error adding user {u} to project {pname}")
                logger.error(e.error_message)
                logger.error("Continuing with next user")
                continue


@click.command(
    "clone-group", short_help="Clones all GitLab projects contained in a subgroup"
)
@gl_url
@gl_token
@gl_config_id
@gl_config_file
@click.option("--name", help="Folder the repositories will be cloned into.")
@click.argument("path")
@click.pass_context
def clone_group(  # noqa: PLR0913
    ctx, gitlab_url, token, config_id, config_file, name, path
):
    """This command clones all GitLab projects present in a subgroup - typically
    used for retrieving assignment repositories.

    \b
    Usage example:
      gl-lab clone-group topic/2023-2024/short/lab05"""

    gl = this_gitlab(ctx, gitlab_url, token, config_id, config_file)
    p = Path(path)
    if name is None:
        name = p.name

    cwd = Path.cwd()
    root = cwd / name

    if root.exists():
        if not root.is_dir():
            logger.error(f"Error: {root} exists and is not a directory")
            return
    else:
        root.mkdir()

    try:
        group = gl.groups.get(str(p))
    except gitlab.exceptions.GitlabError as e:
        click.echo(click.style(f"Error getting  group : {p!s}", fg="red", bold=True))
        click.echo(e.error_message)
        return

    for p in group.projects.list(all=True):
        try:
            output = subprocess.check_output(
                ["git", "clone", p.http_url_to_repo], cwd=root
            )
            click.echo(output.decode("utf-8"))
        except subprocess.CalledProcessError as e:
            logger.error(f"Error cloning project : {p.name}")
            logger.error(e.output.decode("utf-8"))
            return


@click.command(
    "return-as-issue",
    short_help="Creates an issue in the assignment project adding an assessment",
)
@gl_url
@gl_token
@gl_config_id
@gl_config_file
@click.option(
    "--title",
    default="Report feedback",
    show_default=True,
    help="Issue title.",
)
@click.argument("project", required=True)
@click.argument("attachment", type=click.Path(exists=True), required=True)
@click.pass_context
def return_as_issue(  # noqa: PLR0913
    ctx, gitlab_url, token, config_id, config_file, title, project, attachment
):
    """This command creates an issue containing a pdf file for the GitLab project specified - typically
    used for giving feedback to students by sharing the commented report
    including an assessment.

    \b
    Usage example:
    gl-lab return-as-issue topic/2023-2024/short/lab01/group-01 report-group01-commented.pdf"""

    gl = gitlab.Gitlab(url=gitlab_url, private_token=token)
    at = Path(attachment)
    p = gl.projects.get(project)
    uploaded_file = p.upload(at.name, filepath=str(at))
    p.issues.create(
        {
            "title": title,
            "description": uploaded_file["markdown"],
            "assignee_ids": [i.id for i in p.members.list(all=True)],
            "confidential": True,
        }
    )
